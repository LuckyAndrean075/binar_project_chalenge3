import { useState, useEffect } from "react";
import { collection, onSnapshot } from "firebase/firestore";
import { db } from "../firebase";

export function useTodos() {
  const [todos, setTodos] = useState([]);
  const [filterStatus, setFilterStatus] = useState("all");

  useEffect(() => {
    const unsubscribe = onSnapshot(collection(db, "todos"), (snapshot) => {
      const data = snapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));

      // untuk membuat filter berdasarkan value checked
      let filteredTodos = [];
      if (filterStatus === "done") {
        filteredTodos = data.filter((todo) => todo.checked === true);
      } else if (filterStatus === "todo") {
        filteredTodos = data.filter((todo) => todo.checked === false);
      } else {
        filteredTodos = data;
      }

      setTodos(filteredTodos);
    });

    return () => unsubscribe();
  }, [filterStatus]);

  const filterDone = () => {
    setFilterStatus("done");
  };

  const filterTodo = () => {
    setFilterStatus("todo");
  };

  const filterAll = () => {
    setFilterStatus("all");
  };

  return {
    todos,
    filterDone,
    filterTodo,
    filterAll,
  };
}
