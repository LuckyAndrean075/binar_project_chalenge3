import Header from "./components/Header";
import Main from "./components/Main";
import SearchBar from "./components/SearchBar";
import AddNewTask from "./components/AddNewTask";
import FilterTask from "./components/FilterTask";
import TaskList from "./components/TaskList";
import DeleteTask from "./components/DeleteTask";
import { Container } from "@mui/material";

function App() {
  return (
    <Container
      maxWidth="md"
      sx={{
        minHeight: "100vh",
        backgroundColor: "aliceblue",
      }}
    >
      <Header>
        <SearchBar />
        <AddNewTask />
      </Header>

      <Main>
        <FilterTask />
        <TaskList />
        <DeleteTask />
      </Main>
    </Container>
  );
}

export default App;
