import React, { createContext, useState } from "react";
import { useTodos } from "../hooks";

const TodoContext = createContext();

function TodoContextProvider({ children }) {
  const { todos, filterDone, filterTodo, filterAll } = useTodos();

  return (
    <TodoContext.Provider
      value={{
        todos,
        filterDone,
        filterTodo,
        filterAll,
      }}
    >
      {children}
    </TodoContext.Provider>
  );
}

export { TodoContextProvider, TodoContext };
