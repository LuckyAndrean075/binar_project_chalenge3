import { Box, Typography } from "@mui/material";
import React from "react";

function Header({ children }) {
  return (
    <Box pt={5}>
      <Typography variant="h5" fontWeight={600} textAlign="center">
        Todo Search
      </Typography>
      <Box
        sx={{
          p: 2,
          border: "1px solid grey",
          width: "60vw",
          display: "flex",
          gap: 8,
          borderRadius: 1,
        }}
      >
        {children}
      </Box>
    </Box>
  );
}

export default Header;
