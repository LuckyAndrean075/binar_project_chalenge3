import { createTheme } from "@mui/material/styles";
import { red, teal } from "@mui/material/colors";

const theme = createTheme({
  palette: {
    primary: {
      main: teal[400],
    },
    danger: {
      main: red[500],
    },
  },
});

export default theme;
