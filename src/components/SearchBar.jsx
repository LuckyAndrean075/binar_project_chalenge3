import { SearchRounded } from "@mui/icons-material";
import { Button, InputBase, styled, ThemeProvider } from "@mui/material";
import { blue } from "@mui/material/colors";
import React from "react";
import theme from "./theme";

const StyledSearch = styled("div")({
  display: "flex",
  flexDirection: "column",
  flex: 3,
});

const Search = styled("div")(({ theme }) => ({
  backgroundColor: "white",
  borderRadius: theme.shape.borderRadius,
  display: "flex",
  gap: 5,
  marginBottom: "3rem",
}));

const SearchIcons = styled("div")(({ theme }) => ({
  backgroundColor: theme.palette.primary.main,
  borderTopLeftRadius: theme.shape.borderRadius,
  borderBottomLeftRadius: theme.shape.borderRadius,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: 35,
}));
function SearchBar() {
  return (
    <ThemeProvider theme={theme}>
      <StyledSearch>
        <Search>
          <SearchIcons>
            <SearchRounded sx={{ color: blue[50] }} />
          </SearchIcons>
          <InputBase placeholder="Search..." />
        </Search>
        <Button variant="contained">Search</Button>
      </StyledSearch>
    </ThemeProvider>
  );
}

export default SearchBar;
