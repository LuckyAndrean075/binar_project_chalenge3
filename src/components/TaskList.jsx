import { Delete, Edit, LibraryBooksRounded } from "@mui/icons-material";
import {
  Box,
  Button,
  Checkbox,
  IconButton,
  InputBase,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Modal,
  styled,
  ThemeProvider,
  Typography,
} from "@mui/material";
import React, { useContext, useState } from "react";
import theme from "./theme";
import { blue } from "@mui/material/colors";
import { TodoContext } from "../context";
import { db } from "../firebase";
import { deleteDoc, doc, updateDoc } from "firebase/firestore";

//styled Input
const StyledModal = styled(Modal)({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

const StyledInput = styled("div")({
  display: "flex",
  flexDirection: "column",
  flex: 3,
});

const Input = styled("div")(({ theme }) => ({
  backgroundColor: "white",
  borderRadius: theme.shape.borderRadius,
  display: "flex",
  gap: 5,
  marginBottom: "3rem",
  border: "1px solid grey",
}));

const InputIcons = styled("div")(({ theme }) => ({
  backgroundColor: theme.palette.primary.main,
  borderTopLeftRadius: theme.shape.borderRadius,
  borderBottomLeftRadius: theme.shape.borderRadius,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: 35,
}));

function TaskList() {
  const { todos } = useContext(TodoContext);
  const [selectedItem, setSelectedItem] = useState(null);
  const [open, setOpen] = useState(false);
  const [edit, setEdit] = useState("");

  //untuk melakukan penghapusan data berdasarkan id dari data
  const handleDelete = async (todo) => {
    await deleteDoc(doc(db, "todos", todo.id));
  };

  //digunakan untuk mengupdate nilai checked pada firebase sesuai id
  const handleCheckbox = async (todo) => {
    await updateDoc(doc(db, "todos", todo.id), { checked: !todo.checked });
  };

  //untuk menghadel ketika menekan  tombol edit
  const handleOpen = (item) => {
    setSelectedItem(item);
    setEdit(item.text);
    setOpen(true);
  };

  //untuk menghadel ketika melakukan perubahan data
  const handleEditSubmit = async () => {
    if (!selectedItem) return;
    await updateDoc(doc(db, "todos", selectedItem.id), { text: edit });
    setOpen(false);
    setEdit("");
  };

  return (
    <ThemeProvider theme={theme}>
      <List dense sx={{ width: "100%" }}>
        {todos.map((value) => {
          const labelId = `checkbox-list-secondary-label-${value}`;
          return (
            <ListItem
              key={value.id}
              sx={{ border: "1px solid grey", marginBottom: 2 }}
              disablePadding
            >
              <ListItemButton>
                <ListItemText
                  id={labelId}
                  primary={` ${value.text}`}
                  sx={{
                    color: value.checked ? "red" : "inherit",
                    textDecoration: value.checked ? "line-through" : "none",
                  }}
                />
                <ListItemIcon>
                  <Checkbox
                    edge="end"
                    onChange={() => handleCheckbox(value)}
                    checked={value.checked}
                    inputProps={{ "aria-labelledby": labelId }}
                  />
                </ListItemIcon>
                <IconButton onClick={() => handleOpen(value)}>
                  <Edit sx={{ color: theme.palette.warning.light }} />
                </IconButton>
                <IconButton onClick={() => handleDelete(value)}>
                  <Delete sx={{ color: theme.palette.error.main }} />
                </IconButton>
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>
      <StyledModal
        open={open}
        onClose={(e) => setOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            p: 2,
            border: "1px solid grey",
            backgroundColor: "white",
            width: "55vw",
            borderRadius: 1,
          }}
        >
          <Typography variant="h5" fontWeight={600} textAlign="center">
            Todo Change
          </Typography>
          <StyledInput>
            <Input>
              <InputIcons>
                <LibraryBooksRounded sx={{ color: blue[50] }} />
              </InputIcons>
              <InputBase
                value={edit}
                onChange={(e) => setEdit(e.target.value)}
              />
            </Input>
            <Button onClick={handleEditSubmit} variant="contained">
              Save Change
            </Button>
          </StyledInput>
        </Box>
      </StyledModal>
    </ThemeProvider>
  );
}

export default TaskList;
