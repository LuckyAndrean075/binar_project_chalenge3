import styled from "@emotion/styled";
import { Box, Button, ThemeProvider, Typography } from "@mui/material";
import React, { useContext } from "react";
import theme from "./theme";
import { TodoContext } from "../context";

const StyledFilter = styled("div")({
  display: "flex",
  flexDirection: "column",
  marginTop: "2rem",
});

function FilterTask() {
  const { filterDone, filterTodo, filterAll } = useContext(TodoContext);

  return (
    <ThemeProvider theme={theme}>
      <StyledFilter>
        <Typography variant="h5" fontWeight={600} textAlign="center">
          Todo List
        </Typography>
        <Box
          sx={{
            pt: 2,
            display: "flex",
            gap: 2,
            justifyContent: "space-between",
          }}
        >
          <Button variant="contained" sx={{ width: "40%" }} onClick={filterAll}>
            All
          </Button>
          <Button
            variant="contained"
            sx={{ width: "40%" }}
            onClick={filterDone}
          >
            Done
          </Button>
          <Button
            variant="contained"
            sx={{ width: "40%" }}
            onClick={filterTodo}
          >
            Todo
          </Button>
        </Box>
      </StyledFilter>
    </ThemeProvider>
  );
}

export default FilterTask;
