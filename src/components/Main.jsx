import { Box } from "@mui/material";
import React from "react";

function Main({ children }) {
  return (
    <Box
      sx={{
        p: 2,
        width: "60vw",
        display: "flex",
        flexDirection: "column",
        gap: 2,
        borderRadius: 1,
      }}
    >
      {children}
    </Box>
  );
}

export default Main;
