import { LibraryBooksRounded } from "@mui/icons-material";
import {
  Box,
  Button,
  InputBase,
  Modal,
  styled,
  ThemeProvider,
  Typography,
} from "@mui/material";
import { blue } from "@mui/material/colors";
import React, { useState } from "react";
import theme from "./theme";
import { db } from "../firebase";
import { collection, addDoc } from "firebase/firestore";

//styled
const StyledAdd = styled("div")({
  display: "flex",
  flex: 2,
  marginTop: "5rem",
});

const StyledModal = styled(Modal)({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

const StyledInput = styled("div")({
  display: "flex",
  flexDirection: "column",
  flex: 3,
});

const Input = styled("div")(({ theme }) => ({
  backgroundColor: "white",
  borderRadius: theme.shape.borderRadius,
  display: "flex",
  gap: 5,
  marginBottom: "3rem",
  border: "1px solid grey",
}));

const InputIcons = styled("div")(({ theme }) => ({
  backgroundColor: theme.palette.primary.main,
  borderTopLeftRadius: theme.shape.borderRadius,
  borderBottomLeftRadius: theme.shape.borderRadius,
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: 35,
}));

function AddNewTask() {
  const [open, setOpen] = useState(false);
  const [newValue, setNewValue] = useState("");

  const handleOpen = () => {
    setOpen(true);
  };

  //untuk menghadel ketika melakukan penambahan data kedalam  firebase menggunakan addDoc
  //preventDefault() digunakan agar halaman tidak merefresh
  //checked untuk nilai awal saya deklarasikan false yang berarti todo belum selesai dijalankan
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (newValue !== "" && typeof newValue === "string") {
      try {
        const docRef = await addDoc(collection(db, "todos"), {
          text: newValue,
          checked: false,
        });
        setNewValue("");
        setOpen(false);
      } catch (err) {
        console.error(err);
      }
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <StyledAdd>
        <Button onClick={handleOpen} variant="contained" sx={{ width: "100%" }}>
          Add New Task
        </Button>
      </StyledAdd>
      <StyledModal
        open={open}
        onClose={(e) => setOpen(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            p: 2,
            border: "1px solid grey",
            backgroundColor: "white",
            width: "55vw",
            borderRadius: 1,
          }}
        >
          <Typography variant="h5" fontWeight={600} textAlign="center">
            Todo Input
          </Typography>
          <StyledInput>
            <Input>
              <InputIcons>
                <LibraryBooksRounded sx={{ color: blue[50] }} />
              </InputIcons>
              <InputBase
                value={newValue}
                onChange={(e) => setNewValue(e.target.value)}
                placeholder="Input New Task........"
              />
            </Input>
            <Button onClick={handleSubmit} variant="contained">
              Add New Task
            </Button>
          </StyledInput>
        </Box>
      </StyledModal>
    </ThemeProvider>
  );
}

export default AddNewTask;
