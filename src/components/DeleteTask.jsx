import { Box, Button, styled, ThemeProvider, Typography } from "@mui/material";
import React from "react";
import theme from "./theme";
import { db } from "../firebase";
import {
  collection,
  deleteDoc,
  doc,
  getDocs,
  query,
  where,
} from "firebase/firestore";

const StyledDelete = styled("div")({
  display: "flex",
  flexDirection: "column",
  marginTop: "-2rem",
});

function DeleteTask() {
  //digunakan untuk menghapus data berdasarkan nilai checked apakah = true menggunakan where
  const handleDeleteDone = async () => {
    const q = query(collection(db, "todos"), where("checked", "==", true));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((todo) => {
      deleteDoc(doc(db, "todos", todo.id));
    });
  };

  //digunakan untuk menghapus seluruh data dalam firbase
  const handleDeleteAll = async () => {
    const q = query(collection(db, "todos"));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((todo) => {
      deleteDoc(doc(db, "todos", todo.id));
    });
  };
  return (
    <ThemeProvider theme={theme}>
      <StyledDelete>
        <Box
          sx={{
            pt: 2,
            display: "flex",
            gap: 2,
            justifyContent: "space-between",
          }}
        >
          <Button
            onClick={() => handleDeleteDone()}
            variant="contained"
            sx={{ width: "50%", backgroundColor: theme.palette.error.main }}
          >
            Delete Done Task
          </Button>
          <Button
            onClick={() => handleDeleteAll()}
            variant="contained"
            sx={{ width: "50%", backgroundColor: theme.palette.error.main }}
          >
            Delete All Task
          </Button>
        </Box>
      </StyledDelete>
    </ThemeProvider>
  );
}

export default DeleteTask;
