// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBCb-7YnLapjd6e-2m4epGr-AOx01-qMFc",
  authDomain: "todo-app-714fe.firebaseapp.com",
  projectId: "todo-app-714fe",
  storageBucket: "todo-app-714fe.appspot.com",
  messagingSenderId: "41568627816",
  appId: "1:41568627816:web:68c124f7e82273582e1fdd",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { db };
